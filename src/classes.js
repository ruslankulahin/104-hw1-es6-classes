/* ## Теоретичні питання

1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.

  Якщо у об'єкту A прототипом є об'єкт В, та у об'єкту B є якась властивість, 
  об'єкт A успадкує туж саму властивість. 
  І нам не потрібно повторювати всю інформацію про A, яку він успадковує від  B.

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

  super() викликається для того щоб успадкувати та ініціалізувати властивості батьківського класу. 
  При цьому у виклику super() потрібно передати в якості аргументів ті ж самі аргументи, 
  що отримує конструктор класу-нащадка.  */


/* ## Завдання

1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
    Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
2. Створіть гетери та сетери для цих властивостей.
3. Створіть клас Programmer, який успадковуватиметься від класу Employee, 
    і який матиме властивість lang (список мов).
4. Для класу Programmer перезапишіть гетер для властивості salary. 
    Нехай він повертає властивість salary, помножену на 3.
5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.*/


// Клас Employee
class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    // Гетери
    get name() {
        return this._name;
    }
  
    get age() {
        return this._age;
    }
  
    get salary() {
        return this._salary;
    }
  

    // Сетери
    set name(name) {
        this._name = name;
    }
  
    set age(age) {
        this._age = age;
    }
  
    set salary(salary) {
        this._salary = salary;
    }
}
  

  // Клас Programmer, який успадковує Employee
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
    }
  
    // Перезаписуємо гетер для salary,та повертаємо помножений на 3
    get salary() {
      return this._salary * 3;
    }
}

  // Створюємо екземпляри класу Programmer
  const programmer1 = new Programmer("Ruslan", 50, 50000, ["JavaScript", "Java"]);
  const programmer2 = new Programmer("Irina", 43, 60000, ["C++", "PHP"]);
  
  // Виводимо екземпляри у консоль
  console.log({programmer1});
  console.log({programmer2});

  // це поза завданням - перевірка перезапису salary
  console.log(programmer1.salary);
  console.log(programmer2.salary);